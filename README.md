# COMP3931 Individual Project - Describing Object Trajectories in Tracked Video Data
## Overview
This project detects and tracks objects in videos. Based on the tracking data, high level movement description labels are generated and displayed on top of the video.

## Dependencies
This project is compatibile with Python 3.
The code relies on the following dependencies:

* opencv-python
* numpy
* tensorflow
* scipy
* pandas
* seaborn

These can be installed through `pip` by using the requirements file: ```pip3 install -r requirements.txt```

## Installation
1. Clone the repository in a folder.
2. Install the dependencies as mentioned above.

## Running the application
The main application is run by the following command:

```python3 main.py <video>``` on Linux or

`py main.py <video>` on Windows

```video``` is an optional argument that specifies the location of a video that you wish to process. The video will be processed and saved in the same directory under the filename ```<videoname>_movements.mp4```. The video can be viewed once saved.
If this is left empty, the application will attempt to use a live video capture device (webcam) and process the video stream through a user interface. This program can be exited by pressing the ```q``` key.

## File overview

* ```main.py``` The main application.
* ```train.py``` The training application.
* ```test.py``` The test application. **NOTE:** By default testing on videos using the detector has been disabled as it takes around 40 minutes to process. Furthermore, the video files have not been included in this repository due to file size. Re-enabling the tests can be done by uncommenting the relevant code however without the video files this will not work. I have already processed the videos and uploaded them in a demonstration video [here](https://youtu.be/jGpTzIRYQD0).

* ```detector.py``` Handles object detection and tracking.
* ```descriptor.py``` Handles movement description and relevant metric calculations.

The `models` folder contains the models for the neural networks and movement descriptor.

The files in the `deep_sort` and `tools` folder are from the [DeepSORT repository](https://github.com/nwojke/deep_sort) and have been slightly modified.
