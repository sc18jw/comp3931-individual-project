import cv2 as cv
import numpy as np
import detector
import descriptor
import os
import configparser
import time
import datetime
import math
import csv
from collections import deque

# Exports a frame_array as MP4
def save_video(path, video_name, frame_array, fps, width, height):

    print(f"Saving video as {video_name}.mp4...")
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    out = cv.VideoWriter(f"{path}/{video_name}.mp4", fourcc, fps, (width, height))
    for frame in frame_array:
        out.write(frame)
    out.release()
    print("Video successfully saved!")

# Takes a sequence of images, returns array of video frames
# Assumes name of images in directory is in ascending order
def read_video_from_images(path, video_name):

    print(f"Importing {video_name} from images...")

    # Video frames will be stored here
    frame_array = []

    # Sort image sequence
    dir_list = os.listdir(path)
    dir_list.sort()

    # Append to array
    for image in dir_list:
        frame = cv.imread(f"{path}/{image}")
        frame_array.append(frame)

    return frame_array

# Reads video and associated data in MOT format
def read_MOT_video(path):

    # Initialise detector
    det = detector.Detector()

    # Get video meta data
    config = configparser.ConfigParser()
    config.read(f"{path}/seqinfo.ini")
    video_name = config["Sequence"]["name"]
    width = int(config["Sequence"]["imWidth"])
    height = int(config["Sequence"]["imHeight"])
    fps = int(config["Sequence"]["frameRate"])

    # Get video frames
    frame_array = read_video_from_images(f"{path}/img1", video_name)

    # Interval calculations
    dt = 0.5
    interval = int(math.ceil(fps * dt))

    # Process video and measure time taken
    print("Processing video...")
    frame_count = 1
    processed_frames = []
    total_frames = len(frame_array)
    start = time.time()
    for frame in frame_array:
        print(f"{frame_count} of {total_frames} frames")
        processed_frames.append(det.find_objects(frame, interval, dt))
        frame_count += 1
    end = time.time()

    # Format time
    total_duration = datetime.timedelta(seconds=(end - start))
    total_duration -= datetime.timedelta(microseconds=total_duration.microseconds)
    print(f"Total processing time: {total_duration}")

    # Save video to same directory
    save_video(path, "all_movements", frame_array, fps, width, height)

def read_video(path, file):

    # Initialise variables
    det = detector.Detector()
    processed_frames = []
    fps = 30
    dt = 0.5
    interval = fps * dt

    # Read video
    start = time.time()
    cap = cv.VideoCapture(f"{path}/{file}")
    width = int(cap.get(3))
    height = int(cap.get(4))
    print("Processing video...")
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            print("End of stream")
            break
        processed_frames.append(det.find_objects(frame, interval, dt))
    end = time.time()

    # Format time
    total_duration = datetime.timedelta(seconds=(end - start))
    total_duration -= datetime.timedelta(microseconds=total_duration.microseconds)
    print(f"Total processing time: {total_duration}")

    save_video(path, "test_result", processed_frames, fps, width, height)
    cap.release()

def test_descriptor(path):

    models = ["movements_singlewalk.csv", "movements_no_abs.csv", "all_movements.csv"]

    # Read labelled gt file
    gt_file = f"{path}/gt/gt_labelled.txt"

    # Get video meta data
    config = configparser.ConfigParser()
    config.read(f"{path}/seqinfo.ini")
    fps = int(config["Sequence"]["frameRate"])
    name = config["Sequence"]["name"]
    gt = []

    dt = 0.5
    interval = int(dt * fps)

    # Measurements
    correct = 0
    samples = 0

    print(f"Testing {name}\n")

    # Read ground truth file
    with open(gt_file) as file:
        reader = csv.reader(file)
        frame = 0
        # Track previous row and previous track id
        prev_rows = deque()
        prev_id = -1
        for row in reader:
            gt.append(row)

    # Test different models
    for model in models:
        # Initialise descriptor
        desc = descriptor.Descriptor(f"model_data/descriptor/{model}")
        correct = 0
        samples = 0
        classes = {"walking":{"correct":0, "samples":0}, "still":{"correct":0, "samples":0}}
        # Generate labels
        for row in gt:
            # Reset queue if new id
            if int(row[1]) != prev_id:
                prev_id = int(row[1])
                prev_rows.clear()

            # Describe movement
            if len(prev_rows) > interval:
                prev_row = prev_rows.popleft()
                bbox1 = [int(prev_row[2]), int(prev_row[3]), int(prev_row[4]), int(prev_row[5])]
                bbox2 = [int(row[2]), int(row[3]), int(row[4]), int(row[5])]
                velocity = desc.calc_velocity(bbox1, bbox2, dt)
                label = desc.classify_movement(velocity, "person")

                # Update measurements
                if label == row[9]:
                    correct += 1
                    classes[row[9]]["correct"] += 1
                samples += 1
                classes[row[9]]["samples"] += 1

            # Add row to queue
            prev_rows.append(row)

        # Calculate accuracy
        accuracy = correct / samples
        for movement in classes:
            classes[movement]["accuracy"] = classes[movement]["correct"] / classes[movement]["samples"]

        # Output accuracy
        print(f"Model {model}:\nOverall Accuracy: {accuracy} Correct/Samples: {correct}/{samples}")
        for movement in classes:
            print(f"{movement} accuracy: {classes[movement]['accuracy']} Correct/Samples: {classes[movement]['correct']}/{classes[movement]['samples']}")
        print("\n")
    print("\n")

def test_descriptor_time(path):

    models = ["all_movements.csv"]

    # Read labelled gt file
    gt_file = f"{path}/gt/gt.txt"

    # Get video meta data
    config = configparser.ConfigParser()
    config.read(f"{path}/seqinfo.ini")
    fps = int(config["Sequence"]["frameRate"])
    name = config["Sequence"]["name"]
    length = int(config["Sequence"]["seqLength"])
    gt = []

    dt = 0.5
    interval = int(dt * fps)

    video_length_seconds = length / fps
    video_length = datetime.timedelta(seconds=video_length_seconds)

    print(f"Testing {name}")

    # Read ground truth file
    with open(gt_file) as file:
        reader = csv.reader(file)
        frame = 0
        # Track previous row and previous track id
        prev_rows = deque()
        prev_id = -1
        for row in reader:
            gt.append(row)

    # Test different models
    start = time.time()
    for model in models:
        # Initialise descriptor
        desc = descriptor.Descriptor(f"model_data/descriptor/{model}")
        # Generate labels
        for row in gt:
            # Reset queue if new id
            if int(row[1]) != prev_id:
                prev_id = int(row[1])
                prev_rows.clear()

            # Describe movement
            if len(prev_rows) > interval:
                prev_row = prev_rows.popleft()
                bbox1 = [int(prev_row[2]), int(prev_row[3]), int(prev_row[4]), int(prev_row[5])]
                bbox2 = [int(row[2]), int(row[3]), int(row[4]), int(row[5])]
                velocity = desc.calc_velocity(bbox1, bbox2, dt)
                label = desc.classify_movement(velocity, "person")

            # Add row to queue
            prev_rows.append(row)

    # Calculate duration and output to console
    end = time.time()
    total_duration = datetime.timedelta(seconds=(end - start))
    print(f"Time taken to process: {total_duration}\nVideo length: {video_length}\n")

# Main function
if __name__ == "__main__":

    test_data = ["test_data/MOT16-11", "test_data/MOT16-09", "test_data/MOT16-04", "test_data/MOT16-02"]
    start = time.time()
    # Test video processing
    # for path in test_data:
    #     read_MOT_video(path)

    # Test movement descriptor accuracy and timing
    print("Testing accuracy:")
    for path in test_data:
        test_descriptor(path)
    print("Testing timing:")
    for path in test_data:
        test_descriptor_time(path)

    # Calculate overall test duration
    end = time.time()
    total_duration = datetime.timedelta(seconds=(end - start))
    total_duration -= datetime.timedelta(microseconds=total_duration.microseconds)
    print(f"Total time: {total_duration}")
