import csv

# Label walking objects
def label_walking(path, ids):
    ground_truth_file = f"{path}/gt/gt.txt"
    data = []
    with open(ground_truth_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            if int(row[1]) in ids:
                row.append("walking")
                data.append(row)
    return data

# Label still objects
def label_still(path, ids):
    ground_truth_file = f"{path}/gt/gt.txt"
    data = []
    with open(ground_truth_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            if int(row[1]) in ids:
                row.append("still")
                data.append(row)
        return data

# Write to file
def write_file(path, ids):

    walking = label_walking(path, ids["walking"])
    still = label_still(path, ids["still"])

    with open(f"{path}/gt/gt_labelled.txt", "w", newline="") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(walking)
        csv_writer.writerows(still)



ids_1602 = {"walking":[2,8,16,21,37,33,34,35,39,36,38,43,44], "still":[30,25,24,5,66]}
ids_1604 = {"walking":[98, 99, 2, 83, 82, 70, 71, 100, 101, 125, 124, 133, 132, 134, 105, 106, 107, 108], "still":[6, 4, 45, 46, 81, 88, 90, 91, 93, 92, 94, 95]}
ids_1609 = {"walking":[ 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 29, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43], "still":[30,26,28]}
ids_1611 = {"walking":[3, 4, 1, 24, 5, 6, 20, 80, 27, 7, 45, 12, 10, 33, 34, 35, 36, 1, 14, 25], "still":[32, 51]}
write_file("MOT16-09", ids_1609)
write_file("MOT16-04", ids_1604)
write_file("MOT16-02", ids_1602)
write_file("MOT16-11", ids_1611)
