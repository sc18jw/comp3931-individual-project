import cv2 as cv
import numpy as np
import descriptor
from collections import deque
from deep_sort.tracker import Tracker
from deep_sort.detection import Detection
from deep_sort import nn_matching
from tools import generate_detections as gen_det

class Detector:

    def __init__(self):
        # Model locations
        model_path = "model_data/"
        yolo_model_config = model_path + "yolov3.cfg"
        yolo_model_weights = model_path + "yolov3.weights"
        deep_sort_model = model_path + "mars-small128.pb"
        descriptor_model = model_path + "descriptor/all_movements.csv"

        # Descriptor initialisation
        self.desc = descriptor.Descriptor(descriptor_model)

        # Deep SORT initialisation
        max_cosine_distance = 0.5
        nn_budget = None
        metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
        self.tracker = Tracker(metric)
        self.encoder = gen_det.create_box_encoder(deep_sort_model, batch_size=1)

        # Yolo threshold values
        self.conf_threshold = 0.5
        self.NMS_Threshold = 0.3

        # Label names
        classes_file = model_path + "coco.names"
        self.class_names = []
        with open(classes_file, "rt") as file:
            self.class_names = file.read().rstrip('\n').split('\n')

        # Initialise neural network
        self.net_size = 320
        self.net = cv.dnn.readNetFromDarknet(yolo_model_config, yolo_model_weights)
        self.net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
        self.net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

        # Find output layers
        self.layer_names = self.net.getLayerNames()
        self.output_names = [self.layer_names[i[0]-1] for i in self.net.getUnconnectedOutLayers()]

        # Tracks deque
        self.prev_tracks = deque()

    # Draw and label a bbox based off tracker output
    def draw_box(self, frame, track):

        # Unpack bbox coordinates
        colour = (255,0,255)
        bbox = np.round(track.to_tlwh()).astype(int)
        x,y,w,h = bbox[0], bbox[1], bbox[2], bbox[3]

        # Label information
        class_label = f"{track.class_name.upper()} ID: {track.track_id}"

        # Check for movement class, print label
        if track.movement_class != None:
            movement_label = f"Movement: {track.movement_class.upper()}"
            # Change colour of box depending of movement
            if track.class_name == "person":
                if track.movement_class == "walking":
                    colour = (0,255,0)
                elif track.movement_class == "still":
                    colour = (0,0,255)
                elif track.movement_class == "jumping":
                    colour = (0,255,255)
            elif track.class_name == "car":
                if track.movement_class == "stationary":
                    colour = (0,0,255)
            cv.putText(frame, movement_label, (x, y - 40), cv.FONT_HERSHEY_SIMPLEX, 1, colour, 2)

        # Update frame
        cv.rectangle(frame, (x,y), (x+w,y+h), colour, 2)
        cv.putText(frame, class_label, (x, y - 10), cv.FONT_HERSHEY_SIMPLEX, 1, colour, 2)



    # Find objects in a frame and draw bounding box
    def find_objects(self, frame, interval, dt):

        # Initialise lists
        boxes = []
        confs = []
        classes = []
        detections = []

        # Convert frame to blob
        blob = cv.dnn.blobFromImage(frame, 1 / 255, (self.net_size,self.net_size), swapRB=True, crop=False)
        self.net.setInput(blob)
        outputs = self.net.forward(self.output_names)

        # Handle detections, append to lists
        for output in outputs:
            for detection in output:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = float(scores[class_id])
                # Append new bounding box if over confidence threshold
                if confidence > self.conf_threshold:
                    # Get bounding box values
                    fHeight, fWidth = frame.shape[0], frame.shape[1]
                    w = int(detection[2] * fWidth)
                    h = int(detection[3] * fHeight)
                    x = int(detection[0] * fWidth - (w / 2))
                    y = int(detection[1] * fHeight - (h / 2))
                    # Append to lists
                    boxes.append((x, y, w, h))
                    confs.append(confidence)
                    classes.append(self.class_names[class_id])

        # Apply nonmax supression
        indices = cv.dnn.NMSBoxes(boxes, confs, self.conf_threshold, self.NMS_Threshold)
        features = self.encoder(frame, boxes)

        # Loop through filtered boxes, store detection objects
        for i in indices:
            i = i[0]
            detections.append(Detection(boxes[i], confs[i], classes[i], features[i]))

        # Apply Deep SORT
        self.tracker.predict()
        self.tracker.update(detections)

        # Store tracker bbox positions
        cur_track = {}
        for track in self.tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            cur_track[track.track_id] = {"bbox": track.to_tlwh().astype(int), "class_name": track.class_name}

        # Append list of current track locations
        self.prev_tracks.append(cur_track)

        # Calculate bbox velocity over interval
        if len(self.prev_tracks) > interval:
            self.prev_track = self.prev_tracks.popleft()
            for key in self.prev_track:
                # Check for dissapearing objects
                if key in cur_track:
                    velocity = self.desc.calc_velocity(self.prev_track[key]["bbox"], cur_track[key]["bbox"], dt)
                    movement = self.desc.classify_movement(velocity, cur_track[key]["class_name"])

                    # Set movement class
                    for track in self.tracker.tracks:
                        if not track.is_confirmed() or track.time_since_update > 1:
                            continue
                        if track.track_id == key:
                            track.set_movement_class(movement)

        # Draw bbox
        for track in self.tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            self.draw_box(frame, track)

        return frame
