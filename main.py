import cv2 as cv
import numpy as np
import time
import math
import detector
import sys
import ntpath
import os
import datetime

# Reads a video file, processes it, then saves it in the same directory
def process_video(path):

    # Initialise video capture
    cap = cv.VideoCapture(path)
    head, tail = ntpath.split(path)
    video_name = os.path.splitext(tail)[0] + "_movements"
    det = detector.Detector()

    # Initialise variables for video capture
    # Assumes video is 30 fps
    fps = 30
    dt = 0.5
    interval = fps * dt
    start = time.time()
    processed_frames = []
    width = int(cap.get(3))
    height = int(cap.get(4))
    print("Processing video...")

    # Check whether video can be opened
    if not cap.isOpened():
        print("Error opening video capture")
        exit()

    # Read and process video
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            print("End of stream")
            break
        processed_frames.append(det.find_objects(frame, interval, dt))
    cap.release()

    # Save video
    print(f"Saving video as {video_name}.mp4...")
    fourcc = cv.VideoWriter_fourcc(*'mp4v')
    out = cv.VideoWriter(f"{head}/{video_name}.mp4", fourcc, fps, (width, height))
    for frame in processed_frames:
        out.write(frame)
    out.release()
    print("Video successfully saved!")

    # Format time
    end = time.time()
    total_duration = datetime.timedelta(seconds=(end - start))
    total_duration -= datetime.timedelta(microseconds=total_duration.microseconds)
    print(f"Total processing time: {total_duration}")

# Handles webcam input
def process_live_video():

    # Set up video capture
    cap = cv.VideoCapture(0)

    # Time measurements for FPS calculation
    new_frame_time = 0
    prev_frame_time = 0
    fps = 1
    dt = 0.5

    # Frame interval for sampling bbox positions
    interval = 5 # Initial value 5, actual value fps * dt

    det = detector.Detector()

    # Check whether webcam can be used
    if not cap.isOpened():
        print("Error opening video capture")
        exit()

    # Process webcam video
    while True:

        # Read frame from capture and pass to tracker
        new_frame_time = time.time()
        ret, frame = cap.read()
        frame = det.find_objects(frame, interval, dt)

        # Calculate and display FPS
        fps = int(round(1 / (new_frame_time - prev_frame_time)))
        cv.putText(frame, f"{str(fps)} FPS", (5,15), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)

        # Update interval
        interval = int(math.ceil(fps * dt))
        prev_frame_time = new_frame_time

        # Display frame in window
        cv.imshow("Tracker", frame)

        # Exit program
        if(cv.waitKey(1) == ord('q')):
            break

    # Release resources and quit
    cap.release()
    cv.destroyAllWindows()

# Main function
if __name__ == "__main__":

    # Check command line arguments for videos
    if len(sys.argv) > 1:
        videos = sys.argv[1:]
        for video in videos:
            process_video(video)
    else:
        process_live_video()
