import configparser
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

mot_fieldnames = ["frame", "id", "bb_left", "bb_top", "bb_width", "bb_height"]
hmdb_fieldnames = ["frame", "bb_left", "bb_bottom", "bb_right", "bb_top"]
movement_fieldname = "movement_class"
class_fieldname = "class_name"
velocity_fieldnames = ["x_vel","y_vel","x_scale_vel","y_scale_vel"]

# Return data frame and desired frame interval for HMDB data
def read_HMDB_data(path, dt):

    # Create dataframe
    df = pd.read_csv(f"{path}.bb", names=hmdb_fieldnames, delimiter=" ")
    df = df.dropna()
    # Conver to xywh format
    df["bb_width"] = df["bb_right"] - df["bb_left"]
    df["bb_height"] = df["bb_top"] - df["bb_bottom"]
    df = df.drop(columns=["bb_bottom","bb_right"])

    # Calculate interval
    fps = 30
    interval = int(fps * dt)

    return df, interval

# Return data frame and desired frame interval for MOT challenge data
def read_MOT_data(path, dt):

    # Create dataframe
    df = pd.read_csv(f"{path}/gt/gt.txt", names=mot_fieldnames, usecols=[i for i in range(6)])

    # Get fps
    cfg = configparser.ConfigParser()
    cfg.read(f"{path}/seqinfo.ini")
    fps = int(cfg["Sequence"]["frameRate"])
    interval = int(fps * dt)

    return df, interval

# Takes dx, dy, d_pct_width, d_pct_height, bb_width, bb_height
# Calculates velocity relative to bbox size
# Returns dataframe of form: x_vel, y_vel, x_scale_vel, y_scale_vel
def calc_bbox_vel(speed_df, dt):

    # Check for empty dataframe
    if speed_df.empty:
        return
    # Initialise numpy array
    velocities = np.empty((0,4))

    # Calculate velocities
    for row in speed_df.iterrows():
        series = row[1]

        # Find relative velocity in x direction
        rel_dx = abs(series["dx"]) / series["bb_width"]
        x_vel = rel_dx / dt

        # Find relative velocity in y direction
        rel_dy = abs(series["dy"]) / series["bb_height"]
        y_vel = rel_dy / dt

        # Find relative velocity from size
        x_scale_vel = abs(series["d_pct_width"]) / dt
        y_scale_vel = abs(series["d_pct_height"]) / dt

        # Append to numpy array
        velocities = np.append(velocities, np.array([[x_vel, y_vel, x_scale_vel, y_scale_vel]]), axis=0)

    # Create dataframe from numpy array
    result = pd.DataFrame(velocities, columns=velocity_fieldnames)
    return result

# Finds dy, dx, d_pct_widht, d_pct_height of a dataframe
def find_diff(df, interval):

    subject = df

    # Difference in x, y pos over the interval
    dpos_df = subject[["bb_left","bb_top"]].diff(periods=-interval).abs()
    dpos_df.columns = ["dx","dy"]

    # Difference in scale over interval
    dscale_df = subject[["bb_width","bb_height"]].pct_change().abs()
    dscale_df.columns = ["d_pct_width","d_pct_height"]

    # Combine with original data
    speed_df = pd.concat([dpos_df, dscale_df, subject], axis=1)
    speed_df = speed_df.dropna()

    return speed_df

# Takes a dataframe in MOT format, list of bbox ids, frame interval
# Returns a dataframe of velocities
def calc_vel(df, interval, dt, ids=None):

    # Store results
    velocities = pd.DataFrame(columns=velocity_fieldnames)

    # Process single bbox
    if ids is None:
        speed_df = find_diff(df, interval)
        subject_vel = calc_bbox_vel(speed_df, dt)
        velocities = velocities.append(subject_vel, ignore_index=True, sort=False)
        return velocities

    # Process each bbox
    for id in ids:
        # Preprocessing
        subject = df.loc[df["id"] == id]
        subject = subject.sort_values("frame")

        # Combine with original data
        speed_df = find_diff(subject, interval)

        # Calculate velocities for current bbox and append to results
        subject_vel = calc_bbox_vel(speed_df, dt)
        velocities = velocities.append(subject_vel, ignore_index=True, sort=False)

    return velocities

# Generate walking prototype
def train_walking():

    # Open MOT16-09 gt file
    mot16_09 = "test_data/MOT16-09"
    dt = 0.5
    gt, interval = read_MOT_data(mot16_09, dt)

    # Remove unwanted bboxes
    excluded_ids = [30,6,7,1,2,22,31,32,28,25,27,26]
    mask = gt["id"].isin(excluded_ids)
    walking_subjs = gt.loc[~mask]
    ids = walking_subjs["id"].unique()

    velocities = calc_vel(walking_subjs, interval, dt, ids)

    # Open MOT16-02 gt file
    mot16_02 = "test_data/MOT16-02"
    gt, interval = read_MOT_data(mot16_02, dt)

    # Select desired bboxes
    ids = [2,8,16,21,37,33,34,35,39,36]
    mask = gt["id"].isin(ids)
    walking_subjs = gt.loc[mask]

    # Calculate prototype
    results = calc_vel(walking_subjs, interval, dt, ids)
    velocities = velocities.append(results, ignore_index=True, sort=False)
    velocities[movement_fieldname] = "walking"
    mean_v = velocities.mean()
    mean_v = mean_v.to_frame().transpose()

    # Label movement
    mean_v[movement_fieldname] = "walking"
    mean_v[class_fieldname] = "person"

    return mean_v, velocities

# Generate walking towards camera prototype
def train_walking_towards_cam():

    # Open MOT16-09 gt file
    mot16_09 = "test_data/MOT16-09"
    dt = 0.5
    gt, interval = read_MOT_data(mot16_09, dt)

    # Select desired bboxes
    ids = [1,7]
    mask = gt["id"].isin(ids)
    walking_subjs = gt.loc[mask]
    walking_subjs = walking_subjs[walking_subjs["frame"] < 190]

    velocities = calc_vel(walking_subjs, interval, dt, ids)

    # Open MOT16-02 gt file
    mot16_02 = "test_data/MOT16-02"
    gt, interval = read_MOT_data(mot16_02, dt)

    # Select desired bboxes
    ids = [38,43,44]
    mask = gt["id"].isin(ids)
    walking_subjs = gt.loc[mask]

    results = calc_vel(walking_subjs, interval, dt, ids)
    velocities = velocities.append(results, ignore_index=True, sort=False)

    # Open MOT16-04 gt file
    mot16_04 = "test_data/MOT16-04"
    gt, interval = read_MOT_data(mot16_04, dt)

    # Select desired bboxes
    ids = [98, 99, 2, 83, 82, 70, 71, 100, 101, 125, 124, 133, 132, 134, 105, 106, 107, 108]
    mask = gt["id"].isin(ids)
    walking_subjs = gt.loc[mask]

    results = calc_vel(walking_subjs, interval, dt, ids)
    velocities = velocities.append(results, ignore_index=True, sort=False)
    velocities[movement_fieldname] = "walking_towards_cam"

    # Calculate prototype
    mean_v = velocities.mean()
    mean_v = mean_v.to_frame().transpose()

    # Label movement
    mean_v[movement_fieldname] = "walking"
    mean_v[class_fieldname] = "person"

    return mean_v, velocities

# Train standing still prototype
def train_standing_still():

    # Open MOT16-09 gt file
    mot16_09 = "test_data/MOT16-09"
    dt = 0.5
    gt, interval = read_MOT_data(mot16_09, dt)
    # Select ids
    ids = [30,26,28]
    mask = gt["id"].isin(ids)
    subjects = gt.loc[mask]
    # Process data
    velocities = calc_vel(subjects, interval, dt, ids)

    # Open MOT16-02 gt file
    mot16_02 = "test_data/MOT16-02"
    gt, interval = read_MOT_data(mot16_02, dt)
    # Select ids
    ids = [30,25,24,5,66]
    mask = gt["id"].isin(ids)
    subjects = gt.loc[mask]
    # Process data
    results = calc_vel(subjects, interval, dt, ids)
    velocities = velocities.append(results, ignore_index=True, sort=False)

    # Open MOT16-04 gt file
    mot16_04 = "test_data/MOT16-04"
    gt, interval = read_MOT_data(mot16_04, dt)
    # Select ids
    ids = [6, 4, 45, 46, 81, 88, 90, 91, 93, 92, 94, 95]
    mask = gt["id"].isin(ids)
    subjects = gt.loc[mask]

    # Calculate prototype
    results = calc_vel(subjects, interval, dt, ids)
    velocities = velocities.append(results, ignore_index=True, sort=False)
    velocities[movement_fieldname] = "still"
    mean_v = velocities.mean()
    mean_v = mean_v.to_frame().transpose()

    # Label movement
    mean_v[movement_fieldname] = "still"
    mean_v[class_fieldname] = "person"

    return mean_v, velocities

# Train jumping prototype
def train_jumping():

    # Read data
    filepaths = []
    path = "test_data/HMDB51/jump/"
    files = ["DONNIE_DARKO_jump_f_nm_np1_fr_med_7","prelinger_ControlY1950_jump_f_nm_np1_fr_med_12","THE_PROTECTOR_jump_f_nm_np1_ba_bad_11"]
    dt = 0.5
    velocities = pd.DataFrame(columns=velocity_fieldnames)

    # Generate file paths
    for file in files:
        filepaths.append(path + file)

    # Process files
    for filepath in filepaths:
        # This file is in slow motion
        if files[0] in filepath:
            dt = 1
        else:
            dt = 0.5
        # Process data
        subject, interval = read_HMDB_data(filepath, dt)
        result = calc_vel(subject, interval, dt)
        velocities = velocities.append(result, ignore_index=True, sort=False)
    velocities[movement_fieldname] = "jumping"

    # Calculate prototype
    mean_v = velocities.mean()
    mean_v = mean_v.to_frame().transpose()

    mean_v[movement_fieldname] = "jumping"
    mean_v[class_fieldname] = "person"

    return mean_v, velocities

# Returns prototypes for all human related movements
def train_human_movements():

    # Train data
    walking = train_walking()
    walking_towards_cam = train_walking_towards_cam()
    standing_still = train_standing_still()
    jumping = train_jumping()

    # Combine data for graphing
    graph_data = walking[1].append(walking_towards_cam[1], ignore_index=True, sort=False)
    graph_data = graph_data.append(standing_still[1], ignore_index=True, sort=False)
    graph_data = graph_data.append(jumping[1], ignore_index=True, sort=False)

    # Store mean values
    human_movements = pd.DataFrame(columns=velocity_fieldnames)
    human_movements = human_movements.append(walking[0], ignore_index=True, sort=False)
    human_movements = human_movements.append(walking_towards_cam[0], ignore_index=True, sort=False)
    human_movements = human_movements.append(standing_still[0], ignore_index=True, sort=False)
    human_movements = human_movements.append(jumping[0], ignore_index=True, sort=False)

    # Plot graphs
    walking[0][movement_fieldname] += " centroid"
    walking_towards_cam[0][movement_fieldname] = "walking_towards_cam centroid"
    standing_still[0][movement_fieldname] += " centroid"
    jumping[0][movement_fieldname] += " centroid"

    print("Plotting x velocity against y velocity...")
    ax = sns.scatterplot(data=graph_data, x="x_vel", y="y_vel", hue=movement_fieldname, palette=sns.color_palette()[:4], alpha=0.3, linewidth=0.3, edgecolor="black", s=20)
    means = walking[0]
    means = means.append(walking_towards_cam[0], ignore_index=True, sort=False)
    means = means.append(standing_still[0], ignore_index=True, sort=False)
    means = means.append(jumping[0], ignore_index=True, sort=False)
    ax = sns.scatterplot(data=means, x="x_vel", y="y_vel", hue=movement_fieldname, palette=sns.color_palette("bright")[:4], markers=["D","*","^","s"], linewidth=1, edgecolor="black", style=movement_fieldname, s=[100,300,100,100])
    plt.show()

    graph_data = graph_data.append(means, ignore_index=True, sort=False)

    print("Plotting all metrics...")
    ax = sns.PairGrid(graph_data, hue=movement_fieldname, hue_order=["walking", "walking_towards_cam", "still", "jumping",
        "walking centroid", "walking_towards_cam centroid", "still centroid", "jumping centroid"],
        palette=sns.color_palette()[:4] + sns.color_palette("bright")[:4],
        hue_kws={"alpha": [0.3, 0.3, 0.3, 0.3, 1,1,1,1],
        "marker": ["o","o","o","o", "D","*","^","s"],
        "s":[20,20,20,20, 100,300,100,100]})
    ax.map(plt.scatter, linewidth=0.3, edgecolor="black")
    ax.add_legend()
    plt.show()

    return human_movements

# Train stationary movement
def train_stationary_car():

    # Open MOT16-04 gt file
    mot16_04 = "test_data/MOT16-04"
    dt = 0.5
    gt, interval = read_MOT_data(mot16_04, dt)
    # Select ids
    ids = [48]
    mask = gt["id"].isin(ids)
    subjects = gt.loc[mask]
    # Process data
    results = calc_vel(subjects, interval, dt, ids)

    # Calculate prototype
    mean_v = results.mean()
    mean_v = mean_v.to_frame().transpose()

    mean_v[movement_fieldname] = "stationary"
    mean_v[class_fieldname] = "car"

    return mean_v

# Returns prototypes for all car related movements
def train_car_movements():

    # Train data
    stationary = train_stationary_car()
    car_movements = stationary

    return car_movements

# Main function
if __name__ == "__main__":

    # Generate and store all prototypes
    movements = pd.DataFrame(columns=velocity_fieldnames)
    movements = movements.append(train_human_movements(), ignore_index=True, sort=False)
    movements = movements.append(train_car_movements(), ignore_index=True, sort=False)
    movements.to_csv("model_data/descriptor/all_movements.csv")
    print("Training complete!")
