import csv
from scipy.spatial.distance import euclidean

class Descriptor:

    # Initialise model
    def __init__(self, modelpath):
        self.model = []
        with open(modelpath, mode='r') as file:
            # Read trained data as dict
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                # Extract data, place into list
                movement = row["movement_class"]
                velocity = [float(row["x_vel"]), float(row["y_vel"]), float(row["x_scale_vel"]), float(row["y_scale_vel"])]
                class_name = row["class_name"]
                self.model.append({"movement": movement, "velocity": velocity, "class_name": class_name})

    # Classify object movement based on velocity and object class
    def classify_movement(self, velocity, class_name):
        # Find euclidean distance
        distances = {}
        for row in self.model:
            # Filter by class
            if row["class_name"] == class_name:
                # Pair euclidean distance with movement label
                movement = row["movement"]
                distance = euclidean(velocity, row["velocity"])
                distances[movement] = distance

        # No movements for given object class
        if not distances:
            return None

        # Find closest label
        label = min(distances, key=distances.get)
        return label

    # Finds the velocity of a bbox over an interval dt
    # Bbox in format (x,y,w,h)
    def calc_velocity(self, bbox1, bbox2, dt):

        # Prevent division by 0
        for i in range(len(bbox1)):
            if bbox1[i] < 1:
                bbox1[i] = 1
        for i in range(len(bbox2)):
            if bbox2[i] < 1:
                bbox2[i] = 1

        # Unpack variables
        x1, y1, w1, h1 = bbox1[0], bbox1[1], bbox1[2], bbox1[3]
        x2, y2, w2, h2 = bbox2[0], bbox2[1], bbox2[2], bbox2[3]

        # Find differences
        dx = abs(x2 - x1)
        dy = abs(y2 - y1)
        dw = abs(w2 - w1)
        dh = abs(h2 - h1)

        # Calculate difference relative to bbox dimensions
        rel_dx = dx / w1
        rel_dy = dy / h1

        # Calculate percent change for width and height
        d_pct_width = dw / w1
        d_pct_height = dh / h1

        # Calculate velocities
        x_vel = rel_dx / dt
        y_vel = rel_dy / dt
        x_scale_vel = d_pct_width / dt
        y_scale_vel = d_pct_height / dt

        return x_vel, y_vel, x_scale_vel, y_scale_vel
